#pragma once

namespace farming {
  inline constexpr auto token_contract = "eosio.token"_n;
  inline constexpr auto lp_token_contract = "swap.libre"_n;
} // namespace farming