#include <eosio/tester.hpp>

#include <farming/farming.hpp>
#include <token/token.hpp>

// Catch2 unit testing framework. https://github.com/catchorg/Catch2
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

using namespace eosio;
using user_context = test_chain::user_context;

void farming_setup( test_chain &t ) {
  t.set_code( "farm.libre"_n, "farming.wasm" );
}

void token_setup( test_chain &t ) {
  t.create_code_account( "eosio.token"_n );
  t.create_code_account( "swap.libre"_n );

  t.set_code( "eosio.token"_n, "token.wasm" );
  t.set_code( "swap.libre"_n, "token.wasm" );

  t.as( "eosio.token"_n )
      .act< token::actions::create >( "eosio.token"_n,
                                      s2a( "9000000000000.0000 LIBRE" ) );
  t.as( "eosio.token"_n )
      .act< token::actions::issue >( "eosio.token"_n,
                                     s2a( "9000000000000.0000 LIBRE" ),
                                     "" );

  t.as( "swap.libre"_n )
      .with_code( "swap.libre"_n )
      .act< token::actions::create >( "swap.libre"_n,
                                      s2a( "21000000.000000000 BTCUSD" ) );
  t.as( "swap.libre"_n )
      .with_code( "swap.libre"_n )
      .act< token::actions::issue >( "swap.libre"_n,
                                     s2a( "21000000.000000000 BTCUSD" ),
                                     "" );

  t.as( "swap.libre"_n )
      .with_code( "swap.libre"_n )
      .act< token::actions::create >( "swap.libre"_n,
                                      s2a( "1000000000.000000000 BTCLIB" ) );
  t.as( "swap.libre"_n )
      .with_code( "swap.libre"_n )
      .act< token::actions::issue >( "swap.libre"_n,
                                     s2a( "1000000000.000000000 BTCLIB" ),
                                     "" );
}

struct tester {
  test_chain   chain;
  user_context farmlibre = chain.as( "farm.libre"_n );

  user_context alice = chain.as( "alice"_n );
  user_context bob = chain.as( "bob"_n );
  user_context pip = chain.as( "pip"_n );
  user_context egeon = chain.as( "egeon"_n );
  user_context bertie = chain.as( "bertie"_n );
  user_context ahab = chain.as( "ahab"_n );

  tester() {
    chain.create_code_account( "farm.libre"_n );

    farming_setup( chain );
    token_setup( chain );

    for ( auto account : { "reward.libre"_n,
                           "alice"_n,
                           "bob"_n,
                           "pip"_n,
                           "egeon"_n,
                           "bertie"_n,
                           "ahab"_n } ) {
      chain.create_account( account );
    }
  }

  void skip_to( std::string_view time ) {
    uint64_t value;
    check( string_to_utc_microseconds( value,
                                       time.data(),
                                       time.data() + time.size() ),
           "bad time" );

    time_point tp{ microseconds( value ) };

    chain.finish_block();
    auto head_tp = chain.get_head_block_info().timestamp.to_time_point();
    auto skip = ( tp - head_tp ).count() / 1000 - 500;
    chain.start_block( skip );
  }

  void fund_accounts() {
    for ( auto account :
          { "alice"_n, "bob"_n, "pip"_n, "egeon"_n, "bertie"_n, "ahab"_n } ) {
      chain.as( "eosio.token"_n )
          .act< token::actions::transfer >( "eosio.token"_n,
                                            account,
                                            s2a( "1000000000000.0000 LIBRE" ),
                                            "memo" );
      chain.as( "swap.libre"_n )
          .with_code( "swap.libre"_n )
          .act< token::actions::transfer >( "swap.libre"_n,
                                            account,
                                            s2a( "21000.000000000 BTCLIB" ),
                                            "memo" );

      chain.as( "swap.libre"_n )
          .with_code( "swap.libre"_n )
          .act< token::actions::transfer >( "swap.libre"_n,
                                            account,
                                            s2a( "1000000.000000000 BTCUSD" ),
                                            "memo" );
    }
  }

  auto get_balance() const {
    std::map< eosio::name, eosio::asset > result;
    farming::account_table_type           _balance{
        "farm.libre"_n,
        eosio::symbol_code( "BTCLIB" ).raw() };

    for ( auto t : _balance ) {
      result.insert( std::pair( t.account, t.balance ) );
    }

    return result;
  };
};