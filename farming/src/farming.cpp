#include <eosio/system.hpp>
#include <farming/farming.hpp>
#include <math.h>

namespace farming {
  void farming::notify_transfer( eosio::name  from,
                                 eosio::name  to,
                                 eosio::asset quantity,
                                 std::string &memo ) {
    // skip transactions that are not related
    if ( from == get_self() || to != get_self() ) {
      return;
    }

    eosio::check( get_first_receiver() == lp_token_contract,
                  "Contract is not authorized" );

    account_table_type account_tb( get_self(), quantity.symbol.code().raw() );
    auto               account_itr = account_tb.find( from.value );

    if ( account_itr == account_tb.end() ) {
      account_tb.emplace( get_self(), [&]( auto &row ) {
        row.account = from;
        row.balance = quantity;
        row.contract = get_first_receiver();
      } );
    } else {
      account_tb.modify( account_itr, eosio::same_payer, [&]( auto &row ) {
        row.balance += quantity;
      } );
    }

    eosio::action{ { get_self(), "active"_n },
                   get_self(),
                   "stake"_n,
                   std::tuple( from, quantity ) }
        .send();
  }

  void farming::stake( eosio::name account, eosio::asset quantity ) {
    require_auth( get_self() );

    require_recipient( REWARD_CONTRACT );
  }

  void farming::withdraw( eosio::name account, eosio::asset quantity ) {
    require_auth( account );

    account_table_type account_tb( get_self(), quantity.symbol.code().raw() );
    auto               account_itr = account_tb.find( account.value );

    eosio::check( account_itr != account_tb.end(), "Account does not exist" );
    eosio::check( account_itr->balance >= quantity, "Overdrawn balance" );

    account_tb.modify( account_itr, eosio::same_payer, [&]( auto &row ) {
      row.balance -= quantity;
    } );

    eosio::action{ { get_self(), "active"_n },
                   account_itr->contract,
                   "transfer"_n,
                   std::tuple( get_self(),
                               account,
                               quantity,
                               std::string( "return LP tokens" ) ) }
        .send();

    require_recipient( REWARD_CONTRACT );
  }
} // namespace farming

EOSIO_ACTION_DISPATCHER( farming::actions )

EOSIO_ABIGEN( actions( farming::actions ),
              table( "account"_n, farming::account ) )
