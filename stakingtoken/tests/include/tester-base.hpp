#include <eosio/tester.hpp>

#include <stakingtoken.hpp>
#include <token/token.hpp>

// Catch2 unit testing framework. https://github.com/catchorg/Catch2
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

using namespace eosio;
using user_context = test_chain::user_context;

void stakingtoken_setup( test_chain &t ) {
  t.set_code( "stake.libre"_n, "stakingtoken.wasm" );
}

void token_setup( test_chain &t ) {
  t.create_code_account( "eosio.token"_n );
  t.create_code_account( "btc.ptokens"_n );
  t.set_code( "eosio.token"_n, "token.wasm" );
  t.set_code( "btc.ptokens"_n, "token.wasm" );
  t.as( "eosio.token"_n )
      .act< token::actions::create >( "eosio.token"_n,
                                      s2a( "9000000000000.0000 LIBRE" ) );
  t.as( "eosio.token"_n )
      .act< token::actions::issue >( "eosio.token"_n,
                                     s2a( "9000000000000.0000 LIBRE" ),
                                     "" );
  t.as( "btc.ptokens"_n )
      .with_code( "btc.ptokens"_n )
      .act< token::actions::create >( "btc.ptokens"_n,
                                      s2a( "1000000000.000000000 PBTC" ) );
  t.as( "btc.ptokens"_n )
      .with_code( "btc.ptokens"_n )
      .act< token::actions::issue >( "btc.ptokens"_n,
                                     s2a( "1000000000.000000000 PBTC" ),
                                     "" );

  t.as( "btc.ptokens"_n )
      .with_code( "btc.ptokens"_n )
      .act< token::actions::create >( "btc.ptokens"_n,
                                      s2a( "1000000000.000000000 OTHER" ) );
  t.as( "btc.ptokens"_n )
      .with_code( "btc.ptokens"_n )
      .act< token::actions::issue >( "btc.ptokens"_n,
                                     s2a( "1000000000.000000000 OTHER" ),
                                     "" );

  t.as( "eosio.token"_n )
      .act< token::actions::create >( "eosio.token"_n,
                                      s2a( "9000000000000.0000 OTHER" ) );
  t.as( "eosio.token"_n )
      .act< token::actions::issue >( "eosio.token"_n,
                                     s2a( "9000000000000.0000 OTHER" ),
                                     "" );
}

struct tester {
  test_chain   chain;
  user_context stakelibre = chain.as( "stake.libre"_n );
  user_context daolibre = chain.as( "dao.libre"_n );
  user_context bitcoinlibre = chain.as( "bitcoinlibre"_n );

  user_context alice = chain.as( "alice"_n );
  user_context bob = chain.as( "bob"_n );
  user_context pip = chain.as( "pip"_n );
  user_context egeon = chain.as( "egeon"_n );
  user_context bertie = chain.as( "bertie"_n );
  user_context ahab = chain.as( "ahab"_n );

  tester() {
    chain.create_code_account( "stake.libre"_n );

    stakingtoken_setup( chain );
    token_setup( chain );

    for ( auto account : { "dao.libre"_n,
                           "bitcoinlibre"_n,
                           "alice"_n,
                           "bob"_n,
                           "pip"_n,
                           "egeon"_n,
                           "bertie"_n,
                           "ahab"_n } ) {
      chain.create_account( account );
    }
  }

  void skip_to( std::string_view time ) {
    uint64_t value;
    check( string_to_utc_microseconds( value,
                                       time.data(),
                                       time.data() + time.size() ),
           "bad time" );

    time_point tp{ microseconds( value ) };

    chain.finish_block();
    auto head_tp = chain.get_head_block_info().timestamp.to_time_point();
    auto skip = ( tp - head_tp ).count() / 1000 - 500;
    chain.start_block( skip );
  }

  void fund_accounts() {
    for ( auto account :
          { "alice"_n, "bob"_n, "pip"_n, "egeon"_n, "bertie"_n, "ahab"_n } ) {
      chain.as( "eosio.token"_n )
          .act< token::actions::transfer >( "eosio.token"_n,
                                            account,
                                            s2a( "1000000000000.0000 LIBRE" ),
                                            "memo" );
      chain.as( "btc.ptokens"_n )
          .with_code( "btc.ptokens"_n )
          .act< token::actions::transfer >( "btc.ptokens"_n,
                                            account,
                                            s2a( "10000000.000000000 PBTC" ),
                                            "memo" );
      chain.as( "btc.ptokens"_n )
          .with_code( "btc.ptokens"_n )
          .act< token::actions::transfer >( "btc.ptokens"_n,
                                            account,
                                            s2a( "10000000.000000000 OTHER" ),
                                            "memo" );
      chain.as( "eosio.token"_n )
          .act< token::actions::transfer >( "eosio.token"_n,
                                            account,
                                            s2a( "1000000000000.0000 OTHER" ),
                                            "memo" );
    }
  }

  template < typename T >
  libre::stake get_stake_row( const T &table, uint64_t index ) {
    libre::stake result;

    for ( auto item : table ) {
      if ( item.index == index ) {
        return item;
      }
    }

    return result;
  };

  libre::stake get_stake( uint64_t      index,
                          eosio::name &&scope = "stake.libre"_n ) {
    libre::stake_table stake_tb{ "stake.libre"_n, scope.value };

    return get_stake_row( stake_tb, index );
  };

  auto get_stakes( eosio::name &&scope = "stake.libre"_n ) const {
    std::map< uint64_t, asset > result;
    libre::stake_table          stake_tb{ "stake.libre"_n, scope.value };

    for ( auto t : stake_tb ) {
      result.insert( std::pair( t.index, t.payout ) );
    }

    return result;
  };

  auto get_mintbonus() const {
    std::map< uint64_t, asset > result;
    libre::mint_bonus_table mint_tb{ "stake.libre"_n, "stake.libre"_n.value };

    for ( auto t : mint_tb ) {
      result.insert( std::pair( t.index, t.libre_minted ) );
    }

    return result;
  };

  auto get_power() const {
    std::map< name, double > result;
    libre::power_table       power_tb{ "stake.libre"_n, "stake.libre"_n.value };

    for ( auto t : power_tb ) {
      result.insert( std::pair( t.account, t.voting_power ) );
    }

    return result;
  };
};
