#include <tester-base.hpp>

#define CATCH_CONFIG_RUNNER

TEST_CASE( "Basic Stake" ) {
  tester t;

  t.skip_to( "2022-07-04T00:00:00.000" );

  t.fund_accounts();

  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "10.0000 LIBRE" ),
                                           "stakefor:365" );
}

TEST_CASE( "Wrong memo to stakefor" ) {
  tester t;

  t.skip_to( "2022-07-04T00:00:00.000" );

  t.fund_accounts();

  expect( t.alice.trace< token::actions::transfer >( "alice"_n,
                                                     "stake.libre"_n,
                                                     s2a( "10.0000 LIBRE" ),
                                                     "memo" ),
          "Invalid memo: expected format is 'action:arg'" );
  expect( t.alice.trace< token::actions::transfer >( "alice"_n,
                                                     "stake.libre"_n,
                                                     s2a( "10.0000 LIBRE" ),
                                                     "memo:1" ),
          "invalid memo" );
  expect( t.alice.trace< token::actions::transfer >( "alice"_n,
                                                     "stake.libre"_n,
                                                     s2a( "10.0000 LIBRE" ),
                                                     "stakefor:n" ),
          "Invalid number: n" );
  expect( t.alice.trace< token::actions::transfer >( "alice"_n,
                                                     "stake.libre"_n,
                                                     s2a( "10.0000 LIBRE" ),
                                                     "stakefor:-1" ),
          "Invalid number: -1" );
  expect( t.alice.trace< token::actions::transfer >(
              "alice"_n,
              "stake.libre"_n,
              s2a( "10.0000 LIBRE" ),
              "stakefor:18446744073709551616" ),
          "Overflow: 18446744073709551616" );
  expect( t.alice.trace< token::actions::transfer >(
              "alice"_n,
              "stake.libre"_n,
              s2a( "10.0000 LIBRE" ),
              "stakefor:18446744073709551615" ),
          "You can stake up to 1460 days" );
  expect( t.alice.trace< token::actions::transfer >( "alice"_n,
                                                     "stake.libre"_n,
                                                     s2a( "10.0000 LIBRE" ),
                                                     "stakefor:0" ),
          "You must stake for at least 1 day" );
}

TEST_CASE( "Wrong token symbol to stakefor" ) {
  tester t;

  t.skip_to( "2022-07-04T00:00:00.500" );

  t.fund_accounts();

  expect( t.alice.with_code( "btc.ptokens"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "stake.libre"_n,
                                                  s2a( "10.000000000 PBTC" ),
                                                  "stakefor:365" ),
          "Invalid contract" );
  expect( t.alice.trace< token::actions::transfer >( "alice"_n,
                                                     "stake.libre"_n,
                                                     s2a( "10.0000 OTHER" ),
                                                     "stakefor:365" ),
          "Invalid token symbol" );
}

TEST_CASE( "Mixed Full Stake" ) {
  tester t;

  t.skip_to( "2022-07-04T00:00:00.500" );
  t.fund_accounts();

  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "200000000.0000 LIBRE" ),
                                           "stakefor:365" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "100000000.0000 LIBRE" ),
                                           "stakefor:30" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "50000000.0000 LIBRE" ),
                                           "stakefor:90" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "70000000.0000 LIBRE" ),
                                           "stakefor:120" );

  t.skip_to( "2022-08-03T00:00:00.500" );

  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "20000000.0000 LIBRE" ),
                                           "stakefor:365" );
  t.bob.act< token::actions::transfer >( "bob"_n,
                                         "stake.libre"_n,
                                         s2a( "100000000.0000 LIBRE" ),
                                         "stakefor:365" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "1000000000.0000 LIBRE" ),
                                           "stakefor:1460" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "50000000.0000 LIBRE" ),
                                           "stakefor:1" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "70000000.0000 LIBRE" ),
                                           "stakefor:1" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "100000000.0000 LIBRE" ),
                                           "stakefor:20" );
  t.bob.act< token::actions::transfer >( "bob"_n,
                                         "stake.libre"_n,
                                         s2a( "100000000.0000 LIBRE" ),
                                         "stakefor:20" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "100000000.0000 LIBRE" ),
                                           "stakefor:365" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "10.0000 LIBRE" ),
                                           "stakefor:365" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "1000000000.0000 LIBRE" ),
                                           "stakefor:1" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "10000000000.0000 LIBRE" ),
                                           "stakefor:400" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "100.0000 LIBRE" ),
                                           "stakefor:1095" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "15000000.0000 LIBRE" ),
                                           "stakefor:1095" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "17.0000 LIBRE" ),
                                           "stakefor:365" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "30.0000 LIBRE" ),
                                           "stakefor:730" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "90.0000 LIBRE" ),
                                           "stakefor:730" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "100.0000 LIBRE" ),
                                           "stakefor:365" );
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "1000000000000.0000 LIBRE" ),
                                         "stakefor:1460" );

  std::map< uint64_t, asset > expected{
      { 0, s2a( "600000000.0000 LIBRE" ) },
      { 1, s2a( "105299011.5352 LIBRE" ) },
      { 2, s2a( "62864707.4206 LIBRE" ) },
      { 3, s2a( "97373094.7346 LIBRE" ) },
      { 4, s2a( "53107478.7403 LIBRE" ) },
      { 5, s2a( "265537393.7016 LIBRE" ) },
      { 6, s2a( "13875426890.2932 LIBRE" ) },
      { 7, s2a( "50023798.2987 LIBRE" ) },
      { 8, s2a( "70033317.6182 LIBRE" ) },
      { 9, s2a( "102508902.9542 LIBRE" ) },
      { 10, s2a( "102508902.9542 LIBRE" ) },
      { 11, s2a( "265537393.7016 LIBRE" ) },
      { 12, s2a( "26.5537 LIBRE" ) },
      { 13, s2a( "1000475965.9745 LIBRE" ) },
      { 14, s2a( "28943777934.2060 LIBRE" ) },
      { 15, s2a( "939.9768 LIBRE" ) },
      { 16, s2a( "140996524.7761 LIBRE" ) },
      { 17, s2a( "45.1413 LIBRE" ) },
      { 18, s2a( "168.1793 LIBRE" ) },
      { 19, s2a( "504.5381 LIBRE" ) },
      { 20, s2a( "265.5373 LIBRE" ) },
      { 21, s2a( "13875426890293.2736 LIBRE" ) } };

  CHECK( t.get_stakes() == expected );
}

TEST_CASE( "Wrong memo to contributefor" ) {
  tester t;

  t.fund_accounts();

  t.stakelibre.act< libre::actions::init >( eosio::time_point_sec( 1666746820 ),
                                            10 );

  expect( t.alice.with_code( "btc.ptokens"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "stake.libre"_n,
                                                  s2a( "10.000000000 PBTC" ),
                                                  "memo" ),
          "Invalid memo: expected format is 'action:arg'" );
  expect( t.alice.with_code( "btc.ptokens"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "stake.libre"_n,
                                                  s2a( "10.000000000 PBTC" ),
                                                  "memo:1" ),
          "invalid memo" );

  expect( t.alice.with_code( "btc.ptokens"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "stake.libre"_n,
                                                  s2a( "10.000000000 PBTC" ),
                                                  "contributefor:n" ),
          "Invalid number: n" );
  expect( t.alice.with_code( "btc.ptokens"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "stake.libre"_n,
                                                  s2a( "10.000000000 PBTC" ),
                                                  "contributefor:-1" ),
          "Invalid number: -1" );
  expect( t.alice.with_code( "btc.ptokens"_n )
              .trace< token::actions::transfer >(
                  "alice"_n,
                  "stake.libre"_n,
                  s2a( "10.000000000 PBTC" ),
                  "contributefor:18446744073709551616" ),
          "Overflow: 18446744073709551616" );
  expect( t.alice.with_code( "btc.ptokens"_n )
              .trace< token::actions::transfer >(
                  "alice"_n,
                  "stake.libre"_n,
                  s2a( "10.000000000 PBTC" ),
                  "contributefor:18446744073709551615" ),
          "You can stake up to 1460 days" );
  expect( t.alice.with_code( "btc.ptokens"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "stake.libre"_n,
                                                  s2a( "10.000000000 PBTC" ),
                                                  "contributefor:0" ),
          "You must stake for at least 10 days" );
}

TEST_CASE( "Wrong token symbol to contributefor" ) {
  tester t;

  t.fund_accounts();

  expect( t.alice.trace< token::actions::transfer >( "alice"_n,
                                                     "stake.libre"_n,
                                                     s2a( "10.0000 LIBRE" ),
                                                     "contributefor:365" ),
          "Invalid contract" );
  expect( t.alice.with_code( "btc.ptokens"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "stake.libre"_n,
                                                  s2a( "10.000000000 OTHER" ),
                                                  "contributefor:365" ),
          "Invalid token symbol" );
}

TEST_CASE( "Full Contribution" ) {
  tester t;

  t.skip_to( "2022-07-04T06:00:00.000" );

  t.fund_accounts();

  t.stakelibre.act< libre::actions::init >( time_point_sec{ 1656914400 }, 10 );
  t.chain.start_block();
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "0.010000000 PBTC" ),
                                        "contributefor:365" );
  t.bob.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "bob"_n,
                                        "stake.libre"_n,
                                        s2a( "0.200000000 PBTC" ),
                                        "contributefor:30" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "1.000000000 PBTC" ),
                                        "contributefor:90" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "3.000000000 PBTC" ),
                                        "contributefor:90" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "1.000000000 PBTC" ),
                                        "contributefor:365" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "20.000000000 PBTC" ),
                                        "contributefor:365" );

  t.skip_to( "2022-07-14T05:59:59.500" );
  expect( t.stakelibre.trace< libre::actions::mintprocess >( 100 ),
          "The mint rush has not finished yet" );

  t.skip_to( "2022-07-14T06:00:00.000" );
  t.stakelibre.act< libre::actions::mintprocess >( 1 );
  t.chain.start_block();
  t.stakelibre.act< libre::actions::mintprocess >( 1 );
  t.chain.start_block();
  t.stakelibre.act< libre::actions::mintprocess >( 1 );
  t.chain.start_block();
  t.stakelibre.act< libre::actions::mintprocess >( 1 );
  t.chain.start_block();
  t.stakelibre.act< libre::actions::mintprocess >( 1 );
  t.chain.start_block();
  t.stakelibre.act< libre::actions::mintprocess >( 1 );
  t.chain.start_block();

  expect( t.stakelibre.trace< libre::actions::mintprocess >( 1 ),
          "Nothing to do" );

  std::map< uint64_t, asset > expected{ { 0, s2a( "79333.5977 LIBRE" ) },
                                        { 1, s2a( "1586671.9555 LIBRE" ) },
                                        { 2, s2a( "7933359.7778 LIBRE" ) },
                                        { 3, s2a( "23800079.3335 LIBRE" ) },
                                        { 4, s2a( "7933359.7778 LIBRE" ) },
                                        { 5, s2a( "158667195.5573 LIBRE" ) } };

  CHECK( t.get_mintbonus() == expected );
}

TEST_CASE( "Mixed Full Contribution" ) {
  tester t;

  t.skip_to( "2022-07-04T06:00:00.000" );
  t.fund_accounts();

  t.stakelibre.act< libre::actions::init >( time_point_sec{ 1656914400 }, 10 );
  t.chain.start_block();
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "0.010000000 PBTC" ),
                                        "contributefor:365" );
  t.skip_to( "2022-07-05T00:00:00.500" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "0.200000000 PBTC" ),
                                        "contributefor:30" );
  t.skip_to( "2022-07-06T00:00:00.500" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "1.000000000 PBTC" ),
                                        "contributefor:90" );
  t.skip_to( "2022-07-07T00:00:00.500" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "3.000000000 PBTC" ),
                                        "contributefor:90" );
  t.skip_to( "2022-07-08T00:00:00.500" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "1.000000000 PBTC" ),
                                        "contributefor:365" );
  t.skip_to( "2022-07-09T00:00:00.500" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "20.000000000 PBTC" ),
                                        "contributefor:365" );
  t.skip_to( "2022-07-10T00:00:00.500" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "1000.000000000 PBTC" ),
                                        "contributefor:365" );

  t.skip_to( "2022-07-14T05:59:59.500" );
  expect( t.stakelibre.trace< libre::actions::mintprocess >( 100 ),
          "The mint rush has not finished yet" );

  t.skip_to( "2022-07-14T06:00:00.000" );
  t.stakelibre.act< libre::actions::mintprocess >( 100 );

  std::map< uint64_t, asset > expected{ { 0, s2a( "1950.8198 LIBRE" ) },
                                        { 1, s2a( "39016.3966 LIBRE" ) },
                                        { 2, s2a( "195081.9832 LIBRE" ) },
                                        { 3, s2a( "585245.9496 LIBRE" ) },
                                        { 4, s2a( "195081.9832 LIBRE" ) },
                                        { 5, s2a( "3901639.6640 LIBRE" ) },
                                        { 6, s2a( "195081983.2034 LIBRE" ) } };

  CHECK( t.get_mintbonus() == expected );
}

TEST_CASE( "Contribute for and Mint process" ) {
  tester t;

  t.skip_to( "2022-07-04T00:00:00.000" );
  t.fund_accounts();

  t.stakelibre.act< libre::actions::init >( time_point_sec{ 1656892800 }, 365 );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "0.010000000 PBTC" ),
                                        "contributefor:730" );
  t.skip_to( "2022-07-05T00:00:00.000" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "0.200000000 PBTC" ),
                                        "contributefor:430" );
  t.skip_to( "2022-07-06T00:00:00.000" );
  t.alice.with_code( "btc.ptokens"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "stake.libre"_n,
                                        s2a( "1.000000000 PBTC" ),
                                        "contributefor:410" );

  t.skip_to( "2023-07-03T23:59:59.500" );
  expect( t.stakelibre.trace< libre::actions::mintprocess >( 100 ),
          "The mint rush has not finished yet" );

  t.skip_to( "2023-07-04T00:00:00.000" );
  t.stakelibre.act< libre::actions::mintprocess >( 100 );

  std::map< uint64_t, asset > mint_expected{
      { 0, s2a( "1652892.5619 LIBRE" ) },
      { 1, s2a( "33057851.2396 LIBRE" ) },
      { 2, s2a( "165289256.1983 LIBRE" ) } };
  std::map< uint64_t, asset > stake_expected{
      { 0, s2a( "14876033.0571 LIBRE" ) },
      { 1, s2a( "188623918.0900 LIBRE" ) },
      { 2, s2a( "905924079.8435 LIBRE" ) } };

  CHECK( t.get_mintbonus() == mint_expected );
  CHECK( t.get_stakes() == stake_expected );
}

TEST_CASE( "Voting Power" ) {
  tester t;

  t.skip_to( "2022-07-04T00:00:00.000" );
  t.fund_accounts();

  t.stakelibre.act< libre::actions::init >( time_point_sec{ 1656892800 }, 365 );

  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "200000000.0000 LIBRE" ),
                                           "stakefor:365" );
  t.bob.act< token::actions::transfer >( "bob"_n,
                                         "stake.libre"_n,
                                         s2a( "100000000.0000 LIBRE" ),
                                         "stakefor:30" );
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "50000000.0000 LIBRE" ),
                                         "stakefor:90" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "70000000.0000 LIBRE" ),
                                           "stakefor:120" );

  std::map< name, double > expected{ { "alice"_n, 493013698.630136966705322 },
                                     { "bob"_n, 108219178.082191765308380 },
                                     { "pip"_n, 62328767.123287677764893 } };

  t.stakelibre.act< libre::actions::updatevp >( 100 );
  t.chain.start_block();
  expect( t.stakelibre.trace< libre::actions::updatevp >( 100 ),
          "Nothing to do" );

  CHECK( t.get_power() == expected );

  t.skip_to( "2022-07-05T00:00:00.000" );
  t.alice.act< libre::actions::unstake >( "alice"_n, 0 );
  t.stakelibre.act< libre::actions::updatevp >( 4 );

  // since voting power has started, teh values in the power table are not updated until the full voting power
  // calculation is done
  CHECK( t.get_power() == expected );

  t.stakelibre.act< libre::actions::updatevp >( 3 );

  std::map< name, double > expected2{ { "alice"_n, 92821917.808219164609909 },
                                      { "bob"_n, 107945205.479452058672905 },
                                      { "pip"_n, 62191780.821917809545994 } };

  CHECK( t.get_power() == expected2 );

  t.skip_to( "2022-07-06T00:00:00.000" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "25000000.0000 LIBRE" ),
                                           "stakefor:100" );

  t.stakelibre.act< libre::actions::updatevp >( 1 ); // bod, 1
  t.stakelibre.act< libre::actions::updatevp >( 2 ); // (pip, 2) and (alice, 3)
  t.stakelibre.act< libre::actions::updatevp >( 3 ); // alice, 4
  t.chain.start_block();
  t.stakelibre.act< libre::actions::updatevp >(
      4 ); // remove voting power for account that has no stakes

  expect( t.stakelibre.trace< libre::actions::updatevp >( 5 ),
          "Nothing to do" ); // none, updating has finished

  std::map< name, double > expected3{
      { "alice"_n, 124479452.0547945201396942138671875 },
      { "bob"_n, 107671232.876712322235107421875 },
      { "pip"_n, 62054794.52054794132709503173828125 } };

  CHECK( t.get_power() == expected3 );

  t.skip_to( "2022-07-07T00:00:00.000" );

  t.bob.act< libre::actions::unstake >( "bob"_n, 1 );
  t.stakelibre.act< libre::actions::updatevp >( 100 );

  std::map< name, double > expected4{
      { "alice"_n, 124219178.0821917951107025146484375 },
      { "bob"_n, 0.000000000000000000000000000000 },
      { "pip"_n, 61917808.21917808055877685546875 } };

  CHECK( t.get_power() == expected4 );
}

TEST_CASE( "Voting Power on STAKE_COMPLETED" ) {
  tester t;

  t.skip_to( "2022-07-04T00:00:00.000" );
  t.fund_accounts();

  t.stakelibre.act< libre::actions::init >( time_point_sec{ 1656892800 }, 365 );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "200000000.0000 LIBRE" ),
                                           "stakefor:365" );
  t.bob.act< token::actions::transfer >( "bob"_n,
                                         "stake.libre"_n,
                                         s2a( "100000000.0000 LIBRE" ),
                                         "stakefor:30" );

  std::map< name, double > expected{ { "alice"_n, 400'000'000.000000 },
                                     { "bob"_n, 108'219'178.082191765308380 } };

  CHECK( t.get_power() == expected );

  t.skip_to( "2022-08-03T00:00:00.000" );
  t.stakelibre.act< libre::actions::updatevp >( 100 );

  expected["alice"_n] = 383'561'643.83561647;
  expected["bob"_n] = 100'000'000.00000000;

  CHECK( t.get_power() == expected );

  t.skip_to( "2023-07-05T00:00:00.000" );
  t.stakelibre.act< libre::actions::updatevp >( 100 );

  // when a stake has reached the payout date, the voting power value is
  // the amount the account has staked.

  expected["alice"_n] = 200'000'000.000000;
  expected["bob"_n] = 100'000'000.000000;

  CHECK(t.get_power() == expected);

  t.skip_to( "2025-07-04T00:00:00.000" );
  t.stakelibre.act< libre::actions::updatevp >( 100 );
  CHECK(t.get_power() == expected);
}

TEST_CASE( "Stakes re-allocation" ) {
  tester t;

  t.skip_to( "2022-07-04T00:00:00.000" );
  t.fund_accounts();

  t.stakelibre.act< libre::actions::init >( time_point_sec{ 1656892800 }, 365 );

  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "200000000.0000 LIBRE" ),
                                           "stakefor:365" );
  t.bob.act< token::actions::transfer >( "bob"_n,
                                         "stake.libre"_n,
                                         s2a( "100000000.0000 LIBRE" ),
                                         "stakefor:30" );
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "50000000.0000 LIBRE" ),
                                         "stakefor:90" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "70000000.0000 LIBRE" ),
                                           "stakefor:120" );

  t.chain.start_block();

  t.skip_to( "2022-07-05T00:00:00.000" );
  t.alice.act< libre::actions::unstake >( "alice"_n, 0 );

  libre::stake expected{ .index = 0,
                         .account = "alice"_n,
                         .libre_staked = s2a( "200000000.0000 LIBRE" ),
                         .status = libre::stake_status::STAKE_CANCELED };
  auto         returned_stake = t.get_stake( 0, "canceled"_n );

  CHECK( returned_stake.index == expected.index );
  CHECK( returned_stake.account == expected.account );
  CHECK( returned_stake.libre_staked == expected.libre_staked );
  CHECK( returned_stake.status == expected.status );
}