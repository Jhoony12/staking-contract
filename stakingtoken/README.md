# Libre Staking Contract

LIBRE must be `staked` to engage in governance - voting for validators and DAO proposals. In exchange for securing the network, LIBRE holders receive staking rewards in LIBRE. These staking rewards begin high and decrease over time. More [here](https://docs.libre.org/libre-docs/earn/staking).

## Smart Contract Actions

| User         | Action  | Description            | Pre-Conditions        | Post-Conditions    |
| ------------ | ------- | ---------------------- | --------------------- | ------------------ |
| Token Holder | stake   | enter staking contract | user must exist       | tokens staked      |
| Token Holder | unstake | exit staking contract  | user must have staked | user claims tokens |
| Token Holder | claim   | claim rewards and exit | user must have staked | user claims tokens |

## Interacting with the contract

To start the participation process, the user must make a `transfer` from the `eosio.token` contract with the following memo format:

- `<action>:<total_days> = string:uint64_t`

For example:

- `stakefor:365`
- `contributefor:365`

## Payouts

Staking does not compound for multi-year stakes.

## Permissions

The `claim` action acts on behalf of `eosio` account to `issue` and `transfer` new tokens, then, is needed that `eosio` grants the privileges to a specific permission, for this implementation it is the `stake` permission which give access to `stake.libre` contract to perform this specific actions.

Next are the steps to set the permission working:

1. Create the `stake` permission in the `eosio` account (probably with msig since `eosio` is an account controlled by network producers):

```sh
cleos -u <endpoint> set account permission eosio stake '{"threshold": 1, "keys": [], "accounts":[{"permission":{"actor": "stake.libre", "permission":"active"}, "weight": 1}], "waits": [] }' active -p eosio@active
```

2. Grant `issue` privilege to the `stake` permission:

```sh
cleos -u <endpoint> set action permission eosio eosio.token issue stake -p eosio@active
```

3. Grant `transfer` privilege to the `stake` permission:

```sh
cleos -u <endpoint> set action permission eosio eosio.token transfer stake -p eosio@active
```

## Deployed contracts to public networks

The following accounts have been created:

### Libre Testnet

[stake.libre](https://local.bloks.io/?nodeUrl=https%3A%2F%2Ftestnet.libre.org&coreSymbol=LIBRE&systemDomain=eosio&hyperionUrl=https%3A%2F%2Ftestnet.libre.org)

### Libre Mainnet

[stake.libre](https://local.bloks.io/?nodeUrl=https%3A%2F%2Flb.libre.org&coreSymbol=LIBRE&systemDomain=eosio&hyperionUrl=https%3A%2F%2Flb.libre.org)
