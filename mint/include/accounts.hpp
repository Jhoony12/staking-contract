#pragma once

#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>

#include <constants.hpp>
#include <utils.hpp>

namespace mint {
  struct account_v0 {
    eosio::name           owner;
    eosio::asset          mr_contrib_total;
    eosio::asset          daily_share;
    eosio::asset          claimed;
    eosio::asset          unclaimed;
    eosio::time_point_sec last_claim;

    uint64_t primary_key() const { return owner.value; };
  };
  EOSIO_REFLECT( account_v0,
                 owner,
                 mr_contrib_total,
                 daily_share,
                 claimed,
                 unclaimed,
                 last_claim )

  using account_variant = std::variant< account_v0 >;

  struct account {
    account_variant value;
    FORWARD_MEMBERS( value,
                     owner,
                     mr_contrib_total,
                     daily_share,
                     claimed,
                     unclaimed,
                     last_claim )
    FORWARD_FUNCTIONS( value, primary_key )
  };
  EOSIO_REFLECT( account, value )
  using account_table_type = eosio::multi_index< "account"_n, account >;

  struct fetch_account {
    uint64_t last_index;
  };
  EOSIO_REFLECT( fetch_account, last_index )

  struct update_pct : fetch_account {};
  EOSIO_REFLECT( update_pct, last_index )

  struct migrate_complete {};
  EOSIO_REFLECT( migrate_complete )

  using migrate_variant =
      std::variant< fetch_account, update_pct, migrate_complete >;
  using migrate_singleton = eosio::singleton< "migrate"_n, migrate_variant >;

  class accounts {
  private:
    eosio::name        contract;
    account_table_type account_tb;
    migrate_singleton  mig_sing;

  public:
    accounts( eosio::name contract )
        : contract( contract ), account_tb( contract, contract.value ),
          mig_sing( contract, contract.value ) {}

    const account_table_type &get_table() const { return account_tb; }
    void                      on_init();
    uint32_t                  on_migrate( uint32_t max_steps );
    void add_balance( eosio::name account, eosio::asset balance );
    void on_claim( eosio::name account );

    void fetch_accounts( fetch_account &mig, uint32_t &max_steps );
    void calculate_share( update_pct &mig, uint32_t &max_steps );
  };
} // namespace mint