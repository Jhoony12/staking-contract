#pragma once

#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>

#include <constants.hpp>
#include <utils.hpp>

namespace mint {
  struct global_v0 {
    eosio::asset          btc_contributed;
    eosio::asset          total_lp_tokens;
    eosio::time_point_sec mr_end_date;
    uint16_t              days_since_eomr;
  };
  EOSIO_REFLECT( global_v0,
                 btc_contributed,
                 total_lp_tokens,
                 mr_end_date,
                 days_since_eomr )

  using global_variant = std::variant< global_v0 >;
  using global_singleton = eosio::singleton< "global"_n, global_variant >;

  class globals {
  private:
    eosio::name      contract;
    global_singleton global_sing;

  public:
    globals( eosio::name contract )
        : contract( contract ), global_sing( contract, contract.value ) {}

    void      on_init( eosio::asset total_lp_tokens );
    void      increase_day();
    uint16_t  get_days_since_eomr();
    global_v0 global();
  };
} // namespace mint