#pragma once

#include <eosio/eosio.hpp>

#include <constants.hpp>
#include <utils.hpp>

namespace mint {
  struct next_distribution {
    eosio::block_timestamp distribution_time;
  };
  EOSIO_REFLECT( next_distribution, distribution_time )

  struct current_distribution : next_distribution {
    eosio::name last_processed;
  };
  EOSIO_REFLECT( current_distribution, distribution_time, last_processed )

  using distribution_variant =
      std::variant< next_distribution, current_distribution >;
  using distribution_singleton =
      eosio::singleton< "distribution"_n, distribution_variant >;

  class distributions {
  private:
    eosio::name            contract;
    distribution_singleton distribution_sing;

  public:
    distributions( eosio::name contract )
        : contract( contract ), distribution_sing( contract, contract.value ) {}

    bool     setup_distribution();
    uint32_t on_distribute( uint32_t max_steps );

    void enable_distribution();
    bool has_distribution_finished();
  };
} // namespace mint