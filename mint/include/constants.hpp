#pragma once

namespace mint {
  inline constexpr auto     staking_contract = "stake.libre"_n;
  inline constexpr uint16_t total_minting_days = 365;
  inline constexpr auto     swap_account = "swap.libre"_n;
  inline constexpr auto     default_token_symbol = eosio::symbol( "BTCLIB", 6 );
  inline constexpr uint64_t min_token_amount = 1000000;
} // namespace mint

namespace libre {
  enum stake_status : uint64_t {
    STAKE_IN_PROGRESS = 1,
    STAKE_COMPLETED = 2,
    STAKE_CANCELED = 3,
    STAKE_PREVIEW = 4
  };
}