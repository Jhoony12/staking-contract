#include <eosio/tester.hpp>

#include <constants.hpp>
#include <globals.hpp>
#include <mint.hpp>
#include <stakingtoken.hpp>
#include <token/token.hpp>

// Catch2 unit testing framework. https://github.com/catchorg/Catch2
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

using namespace eosio;
using user_context = test_chain::user_context;

void mint_setup( test_chain &t ) { t.set_code( "mint.libre"_n, "mint.wasm" ); }

void token_setup( test_chain &t ) {
  t.create_code_account( "eosio.token"_n );
  t.create_code_account( "btc.ptokens"_n );

  t.set_code( "eosio.token"_n, "token.wasm" );
  t.set_code( "btc.ptokens"_n, "token.wasm" );

  t.as( "eosio.token"_n )
      .act< token::actions::create >( "eosio.token"_n,
                                      s2a( "9000000000000.0000 LIBRE" ) );
  t.as( "eosio.token"_n )
      .act< token::actions::issue >( "eosio.token"_n,
                                     s2a( "9000000000000.0000 LIBRE" ),
                                     "" );
  t.as( "btc.ptokens"_n )
      .with_code( "btc.ptokens"_n )
      .act< token::actions::create >( "btc.ptokens"_n,
                                      s2a( "1000000000.000000000 PBTC" ) );
  t.as( "btc.ptokens"_n )
      .with_code( "btc.ptokens"_n )
      .act< token::actions::issue >( "btc.ptokens"_n,
                                     s2a( "1000000000.000000000 PBTC" ),
                                     "" );

  t.as( "eosio.token"_n )
      .act< token::actions::create >( "eosio.token"_n,
                                      s2a( "9000000000000.0000 OTHER" ) );
  t.as( "eosio.token"_n )
      .act< token::actions::issue >( "eosio.token"_n,
                                     s2a( "9000000000000.0000 OTHER" ),
                                     "" );
}

void mintrush_setup( test_chain &t ) {
  t.create_code_account( "stake.libre"_n );
  t.set_code( "stake.libre"_n, "stakingtoken.wasm" );
}

struct tester {
  test_chain   chain;
  user_context mintlibre = chain.as( "mint.libre"_n );
  user_context stakingtoken = chain.as( "stake.libre"_n );

  user_context alice = chain.as( "alice"_n );
  user_context bob = chain.as( "bob"_n );
  user_context pip = chain.as( "pip"_n );
  user_context egeon = chain.as( "egeon"_n );
  user_context bertie = chain.as( "bertie"_n );
  user_context ahab = chain.as( "ahab"_n );

  tester() {
    chain.create_code_account( "mint.libre"_n );

    mint_setup( chain );
    token_setup( chain );
    mintrush_setup( chain );

    for ( auto account : { "reward.libre"_n,
                           "alice"_n,
                           "bob"_n,
                           "pip"_n,
                           "egeon"_n,
                           "bertie"_n,
                           "ahab"_n } ) {
      chain.create_account( account );
    }
  }

  void skip_to( std::string_view time ) {
    uint64_t value;
    check( string_to_utc_microseconds( value,
                                       time.data(),
                                       time.data() + time.size() ),
           "bad time" );

    time_point tp{ microseconds( value ) };

    chain.finish_block();
    auto head_tp = chain.get_head_block_info().timestamp.to_time_point();
    auto skip = ( tp - head_tp ).count() / 1000 - 500;
    chain.start_block( skip );
  }

  void fund_accounts() {
    for ( auto account :
          { "alice"_n, "bob"_n, "pip"_n, "egeon"_n, "bertie"_n, "ahab"_n } ) {
      chain.as( "eosio.token"_n )
          .act< token::actions::transfer >( "eosio.token"_n,
                                            account,
                                            s2a( "1000000000000.0000 LIBRE" ),
                                            "memo" );
      chain.as( "btc.ptokens"_n )
          .with_code( "btc.ptokens"_n )
          .act< token::actions::transfer >( "btc.ptokens"_n,
                                            account,
                                            s2a( "10000000.000000000 PBTC" ),
                                            "memo" );
      chain.as( "eosio.token"_n )
          .act< token::actions::transfer >( "eosio.token"_n,
                                            account,
                                            s2a( "1000000000000.0000 OTHER" ),
                                            "memo" );
    }
  }

  void setup_contributions() {
    alice.with_code( "btc.ptokens"_n )
        .act< token::actions::transfer >( "alice"_n,
                                          "stake.libre"_n,
                                          s2a( "0.010000000 PBTC" ),
                                          "contributefor:365" );
    bob.with_code( "btc.ptokens"_n )
        .act< token::actions::transfer >( "bob"_n,
                                          "stake.libre"_n,
                                          s2a( "0.200000000 PBTC" ),
                                          "contributefor:30" );
    pip.with_code( "btc.ptokens"_n )
        .act< token::actions::transfer >( "pip"_n,
                                          "stake.libre"_n,
                                          s2a( "1.000000000 PBTC" ),
                                          "contributefor:90" );
    egeon.with_code( "btc.ptokens"_n )
        .act< token::actions::transfer >( "egeon"_n,
                                          "stake.libre"_n,
                                          s2a( "3.000000000 PBTC" ),
                                          "contributefor:90" );
    bertie.with_code( "btc.ptokens"_n )
        .act< token::actions::transfer >( "bertie"_n,
                                          "stake.libre"_n,
                                          s2a( "1.000000000 PBTC" ),
                                          "contributefor:365" );
    ahab.with_code( "btc.ptokens"_n )
        .act< token::actions::transfer >( "ahab"_n,
                                          "stake.libre"_n,
                                          s2a( "20.000000000 PBTC" ),
                                          "contributefor:365" );
  }

  void configure_mintrush() {
    skip_to( "2023-01-07T06:59:59.500" );

    stakingtoken.act< libre::actions::init >(
        eosio::time_point_sec( 1673074800 ), //January 07, 2023 07:00 AM UTC
        10 );
  }

  void distribute( uint32_t batch_size = 256 ) {
    int64_t day_miliseconds = 86400000;
    while ( true ) {
      auto trace = alice.trace< mint::actions::distribute >( batch_size );
      if ( trace.except ) {
        expect( trace, "Distribution has finished" );
        break;
      }
      chain.start_block( day_miliseconds );
    }
  }

  auto get_daily_reward() const {
    std::map< eosio::name, eosio::asset > result;
    mint::account_table_type account_tb{ "mint.libre"_n, "mint.libre"_n.value };

    for ( auto t : account_tb ) {
      result.insert( std::pair( t.owner(), t.daily_share() ) );
    }

    return result;
  };

  auto get_unclaimed() const {
    std::map< eosio::name, eosio::asset > result;
    mint::account_table_type account_tb{ "mint.libre"_n, "mint.libre"_n.value };

    for ( auto t : account_tb ) {
      result.insert( std::pair( t.owner(), t.unclaimed() ) );
    }

    return result;
  };

  auto get_global() const { return mint::globals( "mint.libre"_n ).global(); };
};