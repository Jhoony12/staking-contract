# Mint Contract

## Restrictions

The Smart Contract will be only usable once that mint rush epoch has finished, then it will be ready to be initialized.

## Definitions

Variant Table: A table that can change its base structure depending on the state of the Smart Contract.

## Data Model

The Smart Contract is composed of three tables:

**global**  
Stores the total liquidity tokens to share and addicional information of the mint rush.

**migration**  
Stores information of the migration to move the data from the stake.libre (mintbonus) to mint.libre safety.

**distribution**  
Holds information relative to the date for the next distribution and the last account. It's for the case that the distribution is too big to be done in one step.

**account**  
Store information of the shares amount.

## Actions

To interact with the Smart Contract there are a couple of actions focus in the user role:

### Admin

This role is a privilege access that only the administrator of the `active` key permission will be able to use.

| Action     | Description                                                                                                                                                                 | Pre-Condition                                  | Post-Condition                             |
| ---------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------- | ------------------------------------------ |
| init       | Read the required values from the stake.libre contract and receive the amount of liquidity provider tokens to be shared                                                     | Mint Rush must have concluded                  | Global values are updated                  |
| distribute | Starts the daily distribution and disburse the amount for each account according to their daily share<br /><br />This action needs to be called until “Nothing to do” error | Migration must be finished                     | New share is added to the unclaimed amount |
| claim      | Send the shares to the account                                                                                                                                              | At least one distribution should have occurred | Funds are sent                             |

### Open

This role is an open access that can be used by anyone.

| Action  | Description                                                                                                        | Pre-Condition                                                              | Post-Condition                                                |
| ------- | ------------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------------- | ------------------------------------------------------------- |
| migrate | Load into the Smart Contract the accounts that participated in the mint rush with their corresponding total amount | - Mint Rush must have concluded<br>- mint.libre must have been initialized | mint.libre will be ready to start with the first distribution |

## Flow Example

Once the mint rush step is concluded in the stake.libre, the mint.libre is ready to start working. The admin user will need to initialize the contract with the total LP tokens to be shared in the following 365 days and right after will start the migration from stake.libre to mint.libre through the migrate action. The distribution of the shares will be done in terms of 365 days, each account will get daily:

```sh
daily_share = percentage contributed in the mint rush * total LP tokens / 365
```

Once the first distribution is ready to be claimed, users can call the claim action to get their shares, if there is no share, the smart contract will throw an error with the following message `No funds to claim`.
The distribution of the Smart Contract will finish at the end of the 365 days but people will still be able to claim their shares at any time.

## Pre-Conditions

- Mint rush must have been concluded in the stake.libre contract.

## Interaction Activity

Let's say we have 3 accounts, Alice, Bob and Pip. Alice is the mint.libre admin while Bob and Pip are mint rush users.

1. Mint Rush concludes in the stake.libre contract.
1. Alice initializes (init) the mint.libre contract with the LP tokens to be shared.
1. Alice `migrate` the data from staking to minting contract.
1. After the first 24 hours, the bob (can be any account, no special permission is required) calls `distribute` action to distribute the first round of funds.
1. Bob claims.
1. Bob claims (second claim in the first distribution) but the Smart Contract throws an error saying `No funds to claim`.
1. Pip doesn't claim in the first distribution, so their shares are built up.
1. After 48 hours (2 shares) Pip decides to claim, Pip will get two shares.
1. Bob claims, then he gets only the second share because he had already claimed the first one.
1. After 365 days, the distribution is finished, so the `distribute` action will start throwing out an error that says `Distribution has finished`, but people can still claim their shares.

## Other Notes

- Shares are cumulative, so if you don't claim in the first distribution, you will get the first and second distribution in the second claim and so on.
- It assumes mint rush finishes correctly. This Smart Contract doesn't check if the mint rush was successful or not, it means, it doesn't validate a minimum amount of accounts that participated in the mint rush or things similar to that.
- This Smart Contract only focuses on things related to mint.libre purpose.

## Cleos Commands

The following is the list of cleos commands to interact with the Smart Contract.

init

```sh
cleos push action mint.libre init '{"total_lp_tokens": "0.000000000 BTCLIB"}' -p mint.libre@active
```

migrate

```sh
cleos push action mint.libre migrate '{"max_steps": 100}' -p mint.libre@active
```

distribute

```sh
cleos push action mint.libre distribute '{"max_steps": 100}' -p mint.libre@active
```

claim

```sh
cleos push action mint.libre claim '{"account": "<account>"}' -p mint.libre@active
```
