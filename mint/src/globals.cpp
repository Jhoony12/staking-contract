#include <globals.hpp>
#include <stakingtoken.hpp>

namespace mint {
  void globals::on_init( eosio::asset total_lp_tokens ) {
    eosio::check( !global_sing.exists(), "Contract is already initialized" );
    eosio::check( total_lp_tokens.amount >= min_token_amount,
                  "Invalid token amount" );
    eosio::check( total_lp_tokens.symbol == default_token_symbol,
                  "Invalid token symbol" );

    libre::mintstats_singleton stats_sing( staking_contract, staking_contract.value );
    auto               mint_stats = stats_sing.get();

    eosio::check( mint_stats.end_date.sec_since_epoch() <=
                      eosio::current_time_point().sec_since_epoch(),
                  "Mint rush has not finished yet" );

    auto glb = global_v0{ .btc_contributed = mint_stats.btc_contributed,
                          .total_lp_tokens = total_lp_tokens,
                          .mr_end_date = mint_stats.end_date,
                          .days_since_eomr = 0 };

    global_sing.set( glb, contract );
  }

  void globals::increase_day() {
    auto global = this->global();
    ++global.days_since_eomr;
    global_sing.set( global, contract );
  }

  uint16_t globals::get_days_since_eomr() {
    auto global = this->global();
    return global.days_since_eomr;
  }

  struct global_v0 globals::global() {
    return std::visit( []( const auto &glb ) { return global_v0{ glb }; },
                       global_sing.get_or_default() );
  }
} // namespace mint