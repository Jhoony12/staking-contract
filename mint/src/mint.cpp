#include <accounts.hpp>
#include <distributions.hpp>
#include <globals.hpp>
#include <mint.hpp>

EOSIO_ACTION_DISPATCHER( mint::actions )

EOSIO_ABIGEN( actions( mint::actions ),
              table( "account"_n, mint::account_variant ),
              table( "migrate"_n, mint::migrate_variant ),
              table( "distribution"_n, mint::distribution_variant ),
              table( "global"_n, mint::global_variant ) )
