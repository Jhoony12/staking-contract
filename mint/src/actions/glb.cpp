#include <accounts.hpp>
#include <distributions.hpp>
#include <globals.hpp>
#include <mint.hpp>

namespace mint {
  void minting::init( eosio::asset total_lp_tokens ) {
    require_auth( get_self() );

    globals( get_self() ).on_init( total_lp_tokens );
    accounts( get_self() ).on_init();
  }
} // namespace mint