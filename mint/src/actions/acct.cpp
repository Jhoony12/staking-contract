#include <accounts.hpp>
#include <distributions.hpp>
#include <mint.hpp>

namespace mint {
  void minting::migrate( uint32_t max_steps ) {
    accounts accounts( get_self() );

    eosio::check( max_steps != accounts.on_migrate( max_steps ),
                  "Nothing to do" );
  }

  void minting::distribute( uint32_t max_steps ) {
    distributions distributions( get_self() );

    eosio::check( distributions.on_distribute( max_steps ) != max_steps,
                  "Nothing to do" );
  }

  void minting::claim( eosio::name account ) {
    require_auth( account );

    accounts( get_self() ).on_claim( account );
  }

} // namespace mint