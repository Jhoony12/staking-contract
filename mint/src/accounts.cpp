#include <accounts.hpp>
#include <distributions.hpp>
#include <globals.hpp>
#include <stakingtoken.hpp>

namespace mint {
  void accounts::on_init() {
    eosio::check( !mig_sing.exists(), "Contract is already initialized" );

    mig_sing.get_or_create( contract, fetch_account{} );
  }

  void accounts::fetch_accounts( fetch_account &mig, uint32_t &max_steps ) {
    libre::mint_bonus_table mint_tb( staking_contract, staking_contract.value );
    auto                itr =
        mig.last_index != 0 ? mint_tb.find( mig.last_index ) : mint_tb.begin();

    for ( ; itr != mint_tb.end() && max_steps > 0; ++itr, --max_steps ) {
      auto acc_itr = account_tb.find( itr->account.value );

      if ( acc_itr == account_tb.end() ) {
        account_tb.emplace( contract, [&]( auto &row ) {
          row.owner() = itr->account;
          row.mr_contrib_total() = itr->btc_contributed;
          row.daily_share() = eosio::asset( 0, default_token_symbol );
          row.claimed() = eosio::asset( 0, default_token_symbol );
          row.unclaimed() = eosio::asset( 0, default_token_symbol );
        } );
      } else {
        account_tb.modify( acc_itr, eosio::same_payer, [&]( auto &row ) {
          row.mr_contrib_total() += itr->btc_contributed;
        } );
      }
    }

    if ( itr == mint_tb.end() ) {
      mig_sing.set( update_pct{}, contract );
    } else {
      mig.last_index = itr->index;
      mig_sing.set( mig, contract );
    }
  }

  void accounts::calculate_share( update_pct &mig, uint32_t &max_steps ) {
    auto         glb = globals( contract ).global();
    eosio::asset total_btc_contributed = glb.btc_contributed;
    auto         total_lp_tokens = glb.total_lp_tokens;
    auto acc_itr = mig.last_index != 0 ? account_tb.find( mig.last_index )
                                       : account_tb.begin();

    for ( ; acc_itr != account_tb.end() && max_steps > 0;
          ++acc_itr, --max_steps ) {
      double contribution_pct =
          static_cast< double >( acc_itr->mr_contrib_total().amount ) /
          static_cast< double >( total_btc_contributed.amount );

      account_tb.modify( acc_itr, eosio::same_payer, [&]( auto &row ) {
        row.daily_share() =
            eosio::asset( contribution_pct * total_lp_tokens.amount / 365,
                          default_token_symbol );
      } );
    }

    if ( acc_itr == account_tb.end() ) {
      mig_sing.set( migrate_complete{}, contract );

      distributions( contract ).enable_distribution();
    } else {
      mig.last_index = acc_itr->owner().value;
      mig_sing.set( mig, contract );
    }
  }

  uint32_t accounts::on_migrate( uint32_t max_steps ) {
    eosio::check( mig_sing.exists(),
                  "The Smart Contract is not initialized yet" );
    eosio::check( !std::holds_alternative< migrate_complete >( mig_sing.get() ),
                  "Migration is over" );

    auto migration = mig_sing.get();

    if ( auto *mig = std::get_if< fetch_account >( &migration ) ) {
      fetch_accounts( *mig, max_steps );
    }

    auto migration2 = mig_sing.get();

    if ( auto *mig = std::get_if< update_pct >( &migration2 ) ) {
      calculate_share( *mig, max_steps );
    }

    return max_steps;
  }

  void accounts::add_balance( eosio::name account, eosio::asset balance ) {
    auto itr = account_tb.find( account.value );

    account_tb.modify( itr, eosio::same_payer, [&]( auto &row ) {
      row.unclaimed() += balance;
    } );
  }

  void accounts::on_claim( eosio::name account ) {
    auto itr = account_tb.find( account.value );

    eosio::check( itr != account_tb.end(), "Account does not exist" );
    eosio::check( itr->unclaimed().amount > 0, "No funds to claim" );

    auto unclaimed = itr->unclaimed();

    account_tb.modify( itr, eosio::same_payer, [&]( auto &row ) {
      row.claimed() += unclaimed;
      row.unclaimed().amount = 0;
    } );

    eosio::action{ { contract, "active"_n },
                   swap_account,
                   "transfer"_n,
                   std::tuple( contract,
                               account,
                               unclaimed,
                               std::string( "share from mint.libre " ) ) }
        .send();
  }

} // namespace mint
