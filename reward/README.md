# HOW TO INIT CONTRACTS FOR FARM AND REWARD

1. Make sure permissions are added to issue and transfer LIBRE
2. Check that min_distribution_interval_sec is set to 10800 // 3 hours
3. Check validation eosio: :check( !global_sing.exists() && !distribution_sing.exists is uncommented
4. Run init function for 86400 interval - allowed pools [
   "BTCUSD"
   ] - reward should be 1/2 of bpay per block (20000)
5. Now you can transfer BTCUSD LP tokens with no memo and they will be staked
6. After 24 hours, anyone can call `updateall` to distribute available rewards to update "unclaimed"
7. Users can claim from the unclaimed funds available

## INITIALIZATION

```sh
cleos push transaction '{
    "delay_sec": 0,
    "max_cpu_usage_ms": 0,
    "actions": [
        {
            "account": "reward.libre",
            "name": "init",
            "data": {
                "distribution_interval_sec": 86400,
                "reward_per_block": 2000,
                "allowed_pools": [
                    "BTCUSD"
                ]
            },
            "authorization": [
                {
                    "actor": "reward.libre",
                    "permission": "active"
                }
            ]
        }
    ]
}'
```

# USAGE

### how to stake into farm.libre

```sh
cleos push transaction '{
    "delay_sec": 0,
    "max_cpu_usage_ms": 0,
    "actions": [
        {
            "account": "swap.libre",
            "name": "transfer",
            "data": {
                "from": "libretester2",
                "to": "farm.libre",
                "quantity": "10.0000000 BTCUSD",
                "memo": ""
            },
            "authorization": [
                {
                    "actor": "libretester2",
                    "permission": "active"
                }
            ]
        }
    ]
}'
```

### how to updateall on cron - ANY ACCOUNT CAN RUN

```sh
cleos push transaction '{
    "delay_sec": 0,
    "max_cpu_usage_ms": 0,
    "actions": [
        {
            "account": "reward.libre",
            "name": "updateall",
            "data": {
                "max_steps": 100
            },
            "authorization": [
                {
                    "actor": "bentester",
                    "permission": "active"
                }
            ]
        }
    ]
}'
```

OR

```
cleos push action reward.libre updateall '["100"]' -p reward.libre
```

### how to update block reward every 6 months - this is the first example, later the amount will be 5000

```sh
cleos push transaction '{
    "delay_sec": 0,
    "max_cpu_usage_ms": 0,
    "actions": [
        {
            "account": "reward.libre",
            "name": "setblockrwd",
            "data": {
                "amount": 10000
            },
            "authorization": [
                {
                    "actor": "reward.libre",
                    "permission": "active"
                }
            ]
        }
    ]
}'
```

## Permissions

2 steps - add reward.libre and stake.libre to eosio::stake and then limit the action

### update stake perm on eosio to include reward.libre

```sh
cleos push transaction '{
    "delay_sec": 0,
    "max_cpu_usage_ms": 0,
    "actions": [
        {
            "account": "eosio",
            "name": "updateauth",
            "data": {
                "account": "eosio",
                "permission": "stake",
                "parent": "active",
                "auth": {
                    "threshold": 1,
                    "keys": [],
                    "accounts": [
                        {
                            "permission": {
                                "actor": "reward.libre",
                                "permission": "active"
                            },
                            "weight": 1
                        },
                        {
                            "permission": {
                                "actor": "stake.libre",
                                "permission": "active"
                            },
                            "weight": 1
                        }
                    ],
                    "waits": []
                }
            },
            "authorization": [
                {
                    "actor": "eosio",
                    "permission": "active"
                }
            ]
        }
    ]
}'
```

### limit that permission to 2 actions: issue and transfer

cleos set action permission eosio eosio.token issue stake -p eosio@active
cleos set action permission eosio eosio.token transfer stake -p eosio@active

```sh
cleos push transaction '{
    "delay_sec": 0,
    "max_cpu_usage_ms": 0,
    "actions": [
        {
            "account": "eosio",
            "name": "linkauth",
            "data": {
                "account": "eosio",
                "code": "eosio.token",
                "type": "issue",
                "requirement": "stake"
            },
            "authorization": [
                {
                    "actor": "eosio",
                    "permission": "active"
                }
            ]
        }
    ]
}'

cleos push transaction '{
    "delay_sec": 0,
    "max_cpu_usage_ms": 0,
    "actions": [
        {
            "account": "eosio",
            "name": "linkauth",
            "data": {
                "account": "eosio",
                "code": "eosio.token",
                "type": "transfer",
                "requirement": "stake"
            },
            "authorization": [
                {
                    "actor": "eosio",
                    "permission": "active"
                }
            ]
        }
    ]
}'
```
