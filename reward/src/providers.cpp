#include <providers.hpp>
#include <token/token.hpp>

namespace reward {
  void providers::stake( eosio::name account, eosio::asset quantity ) {
    auto itr = _provider.find( account.value );

    if ( itr != _provider.end() ) {
      _provider.modify( itr, eosio::same_payer, [&]( auto &row ) {
        row.total_staked() += quantity.amount;
      } );
    } else {
      _provider.emplace( contract, [&]( auto &row ) {
        row.account() = account;
        row.total_staked() = quantity.amount;
      } );
    }
  }

  void providers::on_rmprovider( eosio::name account ) {
    auto itr = _provider.find( account.value );

    eosio::check( itr != _provider.end(), "Provider does not exist" );

    _provider.erase( itr );
  }

  void providers::on_claim( eosio::name account ) {
    auto prov_itr = _provider.find( account.value );

    eosio::check( prov_itr != _provider.end(), "Provider does not exist" );
    eosio::check( prov_itr->unclaimed() > 0, "No reward to claim" );

    auto reward = eosio::asset( prov_itr->unclaimed(), base_token_symbol );
    _provider.modify( prov_itr, eosio::same_payer, []( auto &row ) {
      row.claimed() += row.unclaimed();
      row.unclaimed() = 0;
      row.last_claim() = eosio::current_time_point();
    } );

    // TODO: move to constant header file
    auto TOKEN_ISSUER = "eosio"_n;
    auto SUPPORTED_TOKEN_CONTRACT = "eosio.token"_n;

    eosio::action( eosio::permission_level{ TOKEN_ISSUER, "stake"_n },
                   eosio::name( SUPPORTED_TOKEN_CONTRACT ),
                   eosio::name( "issue" ),
                   std::tuple( TOKEN_ISSUER,
                               reward,
                               "reward for " + account.to_string() ) )
        .send();

    eosio::action( eosio::permission_level{ TOKEN_ISSUER, "stake"_n },
                   eosio::name( SUPPORTED_TOKEN_CONTRACT ),
                   eosio::name( "transfer" ),
                   std::tuple( TOKEN_ISSUER,
                               account,
                               reward,
                               "reward for " + account.to_string() ) )
        .send();
  }

  void providers::set_staked( eosio::name account, uint64_t amount ) {
    _provider.modify( _provider.get( account.value ),
                      eosio::same_payer,
                      [&]( auto &row ) { row.total_staked() = amount; } );
  }

  void providers::sub_staked( eosio::name account, uint64_t amount ) {
    _provider.modify( _provider.get( account.value ),
                      eosio::same_payer,
                      [&]( auto &row ) { row.total_staked() -= amount; } );
  }

  void providers::add_balance( eosio::name account, uint64_t amount ) {
    _provider.modify( _provider.get( account.value ),
                      eosio::same_payer,
                      [&]( auto &row ) { row.unclaimed() += amount; } );
  }

  void providers::sub_balance( eosio::name account, uint64_t amount ) {
    _provider.modify( _provider.get( account.value ),
                      eosio::same_payer,
                      [&]( auto &row ) { row.unclaimed() -= amount; } );
  }

  void providers::on_clearall() {
    auto itr = _provider.begin();

    while ( itr != _provider.end() ) {
      itr = _provider.erase( itr );
    }
  }
}; // namespace reward