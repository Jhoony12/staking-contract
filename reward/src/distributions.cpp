#include <distributions.hpp>
#include <farming/farming.hpp>
#include <providers.hpp>
#include <reward.hpp>

namespace reward {
  void
  distributions::on_init( uint32_t distribution_interval_sec,
                          uint64_t reward_per_block,
                          std::vector< eosio::symbol_code > allowed_pools ) {
    eosio::check( !global_sing.exists() && !distribution_sing.exists() &&
                      !state_sing.exists(),
                  "Contract is already initialized" );

    eosio::time_point_sec ct = eosio::current_time_point();

    global_sing.set(
        global_v0{ .distribution_interval_sec = distribution_interval_sec,
                   .reward_per_block = reward_per_block,
                   .allowed_pools = allowed_pools },
        contract );
    distribution_sing.set(
        next_distribution{ .distribution_time =
                               ct + distribution_interval_sec },
        contract );
    state_sing.set( reward_distribution{}, contract );
  }

  void distributions::on_setinterval( uint32_t time_sec ) {
    auto global = this->global();
    global.distribution_interval_sec = time_sec;

    global_sing.set( global, contract );
  }

  void distributions::on_setblockrwd( uint64_t amount ) {
    auto global = this->global();
    global.reward_per_block = amount;

    global_sing.set( global, contract );
  }

  void distributions::on_addpool( eosio::symbol_code pool ) {
    auto global = this->global();
    auto itr = std::find( begin( global.allowed_pools ),
                          end( global.allowed_pools ),
                          pool );

    eosio::check( itr == std::end( global.allowed_pools ),
                  "Pool already exist" );

    global.allowed_pools.push_back( pool );
    global_sing.set( global, contract );
  }

  void distributions::on_rmpool( eosio::symbol_code pool ) {
    eosio::check( is_next_distribution(),
                  "Cannot remove pool during distribution" );

    auto global = this->global();
    auto itr = std::find( begin( global.allowed_pools ),
                          end( global.allowed_pools ),
                          pool );

    eosio::check( itr != std::end( global.allowed_pools ),
                  "Pool does not exist" );

    global.allowed_pools.erase( itr );
    global_sing.set( global, contract );
  }

  bool distributions::setup_distribution() {
    auto dist_sing = distribution_sing.get();
    auto glb_sing = this->global();

    if ( !glb_sing.allowed_pools.size() ) {
      return false;
    }

    if ( auto *dist = std::get_if< next_distribution >( &dist_sing ) ) {
      auto dist_time = dist->distribution_time.sec_since_epoch();
      auto ct_sse =
          eosio::current_block_time().to_time_point().sec_since_epoch();
      if ( dist_time <= ct_sse ) {
        uint64_t total_paid_blocks = glb_sing.distribution_interval_sec * 2;
        uint64_t reward_per_pool = total_paid_blocks *
                                   glb_sing.reward_per_block /
                                   glb_sing.allowed_pools.size();

        dist->pools.clear();
        auto new_dist = current_distribution{ { *dist } };
        new_dist.reward_per_block =
            eosio::asset( glb_sing.reward_per_block, base_token_symbol );
        new_dist.total_reward =
            eosio::asset( total_paid_blocks * glb_sing.reward_per_block,
                          base_token_symbol );

        for ( auto pool_code : glb_sing.allowed_pools ) {
          new_dist.pools.push_back(
              pool{ .amount = reward_per_pool, .symbol_code = pool_code } );
        }

        distribution_sing.set( new_dist, contract );

        return true;
      }
    }

    return false;
  }

  void distributions::distribute_reward( uint32_t             &max_steps,
                                         current_distribution &curr_dist,
                                         reward_distribution  &st_rwd ) {
    auto pool_itr =
        st_rwd.last_pool == eosio::symbol_code{}
            ? curr_dist.pools.begin()
            : std::find_if( curr_dist.pools.begin(),
                            curr_dist.pools.end(),
                            [&]( auto pool ) {
                              return st_rwd.last_pool == pool.symbol_code;
                            } );
    for ( ; pool_itr != curr_dist.pools.end() && max_steps > 0; ) {
      auto stats_itr = stats_tb.find( pool_itr->symbol_code.raw() );

      if ( stats_itr == stats_tb.end() || stats_itr->balance().amount <= 0 ) {
        ++pool_itr;

        continue;
      }

      providers providers{ contract, pool_itr->symbol_code };
      auto     &_provider = providers.get_table();

      if ( st_rwd.last_pool == eosio::symbol_code{} ) {
        eosio::action{
            { contract, "active"_n },
            contract,
            "receipthdr"_n,
            std::tuple( curr_dist.distribution_time,
                        curr_dist.reward_per_block,
                        eosio::asset( pool_itr->amount, base_token_symbol ) ) }
            .send();
      }

      auto provider_itr = st_rwd.last_account != eosio::name{}
                              ? _provider.find( st_rwd.last_account.value )
                              : _provider.begin();

      for ( ; provider_itr != _provider.end() && max_steps > 0;
            ++provider_itr, --max_steps ) {
        if ( provider_itr->total_staked() == 0 ) {
          continue;
        }

        double reward_pct = (double)provider_itr->total_staked() /
                            (double)stats_itr->balance().amount;
        uint64_t reward = pool_itr->amount * reward_pct;
        providers.add_balance( provider_itr->account(), reward );

        eosio::action{ { contract, "active"_n },
                       contract,
                       "receipt"_n,
                       std::tuple( curr_dist.distribution_time,
                                   pool_itr->symbol_code.to_string(),
                                   provider_itr->account(),
                                   reward_pct,
                                   eosio::asset( reward, base_token_symbol ) ) }
            .send();
      }

      if ( provider_itr == _provider.end() ) {
        ++pool_itr;
        st_rwd.last_account = eosio::name{};
      } else {
        st_rwd.last_account = provider_itr->account();
      }
    }

    st_rwd.last_pool = pool_itr != curr_dist.pools.end() ? pool_itr->symbol_code
                                                         : eosio::symbol_code{};
  }

  uint32_t distributions::update_reward( uint32_t max_steps ) {
    if ( max_steps && setup_distribution() ) {
      --max_steps;
    }

    auto dist_sing = distribution_sing.get();

    if ( auto *dist = std::get_if< current_distribution >( &dist_sing ) ) {
      auto st_sing = state_sing.get();

      if ( auto *state = std::get_if< reward_distribution >( &st_sing ) ) {
        distribute_reward( max_steps, *dist, *state );

        if ( state->last_pool == eosio::symbol_code{} ) {
          auto glb_sing = this->global();
          auto next_dist_time =
              dist->distribution_time + glb_sing.distribution_interval_sec;
          state_sing.set( reward_distribution{}, contract );
          distribution_sing.set(
              next_distribution{ .distribution_time = next_dist_time },
              contract );
        } else {
          state_sing.set( *state, contract );
          distribution_sing.set( *dist, contract );
        }
      }
    }

    return max_steps;
  }

  void distributions::add_balance( eosio::asset quantity ) {
    auto itr = stats_tb.find( quantity.symbol.code().raw() );

    if ( itr == stats_tb.end() ) {
      stats_tb.emplace( contract, [&]( auto &row ) {
        row.value = stats_v0{ .balance = quantity };
      } );
    } else {
      stats_tb.modify( itr, eosio::same_payer, [&]( auto &row ) {
        row.balance() += quantity;
      } );
    }
  }

  void distributions::sub_balance( eosio::asset quantity ) {
    auto itr = stats_tb.find( quantity.symbol.code().raw() );

    if ( itr != stats_tb.end() ) {
      stats_tb.modify( itr, eosio::same_payer, [&]( auto &row ) {
        row.balance() -= quantity;
      } );
    } else {
      eosio::check( false, "Stats balance got corrupted" );
    }
  }

  bool distributions::is_next_distribution() {
    return std::holds_alternative< next_distribution >(
        distribution_sing.get() );
  }

  bool distributions::is_valid_pool( eosio::symbol_code pool ) {
    auto global = this->global();

    return std::any_of( global.allowed_pools.begin(),
                        global.allowed_pools.end(),
                        [&]( auto item ) { return item == pool; } );
  }

  void distributions::on_clearall() {
    global_sing.remove();
    distribution_sing.remove();
  }

  struct global_v0 distributions::global() {
    return std::visit( []( const auto &global ) { return global_v0{ global }; },
                       global_sing.get_or_default() );
  }
} // namespace  reward
