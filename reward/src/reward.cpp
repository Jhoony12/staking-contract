#include <reward.hpp>

EOSIO_ACTION_DISPATCHER( reward::actions )

EOSIO_ABIGEN( actions( reward::actions ),
              table( "global", reward::global_variant ),
              table( "distribution", reward::distribution_variant ),
              table( "state", reward::state_variant ),
              table( "stats", reward::stats_variant ),
              table( "provider", reward::provider_variant ) )
