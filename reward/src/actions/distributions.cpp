#include <distributions.hpp>
#include <providers.hpp>
#include <reward.hpp>

namespace reward {
  void rewards::init( uint32_t distribution_interval_sec,
                      uint64_t reward_per_block,
                      std::vector< eosio::symbol_code > allowed_pools ) {
    require_auth( get_self() );

    eosio::check( distribution_interval_sec >= min_distribution_interval_sec,
                  "Unable to configure distribution in less than 3 hours" );
    eosio::check( allowed_pools.size(), "Need to set allowed poools" );

    distributions{ get_self() }.on_init( distribution_interval_sec,
                                         reward_per_block,
                                         allowed_pools );
  }

  void rewards::receipthdr( eosio::time_point_sec distribution_time,
                            eosio::asset          reward_per_block,
                            eosio::asset          total_pool_reward ) {
    require_auth( get_self() );
  }

  void rewards::receipt( eosio::time_point_sec distribution_time,
                         std::string          &pool_name,
                         eosio::name           account,
                         double                reward_pct,
                         eosio::asset          reward ) {
    require_auth( get_self() );
  }

  void rewards::setinterval( uint32_t time_sec ) {
    require_auth( get_self() );

    eosio::check( time_sec >= min_distribution_interval_sec,
                  "Unable to configure distribution in less than 3 hours" );

    distributions{ get_self() }.on_setinterval( time_sec );
  }

  void rewards::setblockrwd( uint64_t amount ) {
    require_auth( get_self() );

    distributions{ get_self() }.on_setblockrwd( amount );
  }

  void rewards::addpool( eosio::symbol_code pool ) {
    require_auth( get_self() );

    distributions{ get_self() }.on_addpool( pool );
  }

  void rewards::rmpool( eosio::symbol_code pool ) {
    require_auth( get_self() );

    distributions{ get_self() }.on_rmpool( pool );
  }

  void rewards::updateall( uint32_t max_steps ) {
    distributions distributions{ get_self() };

    eosio::check( distributions.update_reward( max_steps ) != max_steps,
                  "Nothing to do" );
  }
} // namespace reward