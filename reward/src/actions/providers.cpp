#include <distributions.hpp>
#include <providers.hpp>
#include <reward.hpp>

namespace reward {
  void rewards::notify_stake( eosio::name account, eosio::asset quantity ) {
    providers     providers{ get_self(), quantity.symbol.code() };
    distributions distributions{ get_self() };

    // QUESTION: if someone transfer to the farm.libre contract an asset that is
    // outside of its whitelisted pool, what should happen? decline the transfer?

    eosio::check( distributions.is_valid_pool( quantity.symbol.code() ),
                  "Pool is not accepted" );

    providers.stake( account, quantity );
    distributions.add_balance( quantity );
  }

  void rewards::notify_withdraw( eosio::name account, eosio::asset quantity ) {
    providers     providers{ get_self(), quantity.symbol.code() };
    distributions distributions{ get_self() };

    providers.sub_staked( account, quantity.amount );
    distributions.sub_balance( quantity );
  }

  void rewards::claim( eosio::symbol_code pool, eosio::name account ) {
    require_auth( account );

    providers providers{ get_self(), pool };

    providers.on_claim( account );
  }
} // namespace  reward