#include <eosio/tester.hpp>

#include <distributions.hpp>
#include <farming/farming.hpp>
#include <reward.hpp>
#include <token/token.hpp>

// Catch2 unit testing framework. https://github.com/catchorg/Catch2
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

using namespace eosio;
using user_context = test_chain::user_context;

void reward_setup( test_chain &t ) {
  t.set_code( "reward.libre"_n, "reward.wasm" );
}

void token_setup( test_chain &t ) {
  t.create_code_account( "eosio.token"_n );
  t.create_code_account( "swap.libre"_n );

  t.set_code( "eosio.token"_n, "token.wasm" );
  t.set_code( "swap.libre"_n, "token.wasm" );

  t.as( "eosio.token"_n )
      .act< token::actions::create >( "eosio.token"_n,
                                      s2a( "9000000000000.0000 LIBRE" ) );
  t.as( "eosio.token"_n )
      .act< token::actions::issue >( "eosio.token"_n,
                                     s2a( "9000000000000.0000 LIBRE" ),
                                     "" );

  t.as( "swap.libre"_n )
      .with_code( "swap.libre"_n )
      .act< token::actions::create >( "swap.libre"_n,
                                      s2a( "21000000.000000000 BTCUSD" ) );
  t.as( "swap.libre"_n )
      .with_code( "swap.libre"_n )
      .act< token::actions::issue >( "swap.libre"_n,
                                     s2a( "21000000.000000000 BTCUSD" ),
                                     "" );

  t.as( "swap.libre"_n )
      .with_code( "swap.libre"_n )
      .act< token::actions::create >( "swap.libre"_n,
                                      s2a( "1000000000.000000000 BTCLIB" ) );
  t.as( "swap.libre"_n )
      .with_code( "swap.libre"_n )
      .act< token::actions::issue >( "swap.libre"_n,
                                     s2a( "1000000000.000000000 BTCLIB" ),
                                     "" );
}

void farm_setup( test_chain &t ) {
  t.create_code_account( "farm.libre"_n );
  t.create_code_account( "farm.fake"_n );

  t.set_code( "farm.libre"_n, "farming.wasm" );
  t.set_code( "farm.fake"_n, "farming.wasm" );
}

struct tester {
  test_chain   chain;
  user_context rewardlibre = chain.as( "reward.libre"_n );
  user_context farmlibre = chain.as( "farm.libre"_n );
  user_context farmfake = chain.as( "farm.fake"_n );

  user_context alice = chain.as( "alice"_n );
  user_context bob = chain.as( "bob"_n );
  user_context pip = chain.as( "pip"_n );
  user_context egeon = chain.as( "egeon"_n );
  user_context bertie = chain.as( "bertie"_n );
  user_context ahab = chain.as( "ahab"_n );

  tester() {
    chain.create_code_account( "reward.libre"_n );

    reward_setup( chain );
    token_setup( chain );
    farm_setup( chain );

    for ( auto account :
          { "alice"_n, "bob"_n, "pip"_n, "egeon"_n, "bertie"_n, "ahab"_n } ) {
      chain.create_account( account );
    }
  }

  void skip_to( std::string_view time ) {
    uint64_t value;
    check( string_to_utc_microseconds( value,
                                       time.data(),
                                       time.data() + time.size() ),
           "bad time" );

    time_point tp{ microseconds( value ) };

    chain.finish_block();
    auto head_tp = chain.get_head_block_info().timestamp.to_time_point();
    auto skip = ( tp - head_tp ).count() / 1000 - 500;
    chain.start_block( skip );
  }

  void fund_accounts() {
    for ( auto account :
          { "alice"_n, "bob"_n, "pip"_n, "egeon"_n, "bertie"_n, "ahab"_n } ) {
      chain.as( "eosio.token"_n )
          .act< token::actions::transfer >( "eosio.token"_n,
                                            account,
                                            s2a( "1000000000000.0000 LIBRE" ),
                                            "memo" );
      chain.as( "swap.libre"_n )
          .with_code( "swap.libre"_n )
          .act< token::actions::transfer >( "swap.libre"_n,
                                            account,
                                            s2a( "21000.000000000 BTCLIB" ),
                                            "memo" );

      chain.as( "swap.libre"_n )
          .with_code( "swap.libre"_n )
          .act< token::actions::transfer >( "swap.libre"_n,
                                            account,
                                            s2a( "1000000.000000000 BTCUSD" ),
                                            "memo" );
    }
  }

  void fund_account( eosio::name account ) {
    chain.as( "eosio.token"_n )
        .act< token::actions::transfer >( "eosio.token"_n,
                                          account,
                                          s2a( "1000000000000.0000 LIBRE" ),
                                          "memo" );
    chain.as( "swap.libre"_n )
        .with_code( "swap.libre"_n )
        .act< token::actions::transfer >( "swap.libre"_n,
                                          account,
                                          s2a( "21000.000000000 BTCLIB" ),
                                          "memo" );

    chain.as( "swap.libre"_n )
        .with_code( "swap.libre"_n )
        .act< token::actions::transfer >( "swap.libre"_n,
                                          account,
                                          s2a( "1000000.000000000 BTCUSD" ),
                                          "memo" );
  }

  template < typename T, typename Y >
  Y get_row( const T &table, eosio::name account ) {
    Y result;

    for ( auto item : table ) {
      auto row = std::get< Y >( item.value );
      if ( row.account == account ) {
        return row;
      }
    }

    return result;
  };

  auto get_provider_row( eosio::name account ) {
    reward::provider_table_type provider_tb(
        "reward.libre"_n,
        eosio::symbol_code( "BTCLIB" ).raw() );

    return get_row< reward::provider_table_type, reward::provider_v0 >(
        provider_tb,
        account );
  }

  template < typename T, typename Y >
  Y get_singleton( eosio::name code, uint64_t scope ) {
    T table( code, scope );

    return std::get< Y >( table.get() );
  }

  auto get_globals() {
    reward::global_singleton global_sing( "reward.libre"_n,
                                          "reward.libre"_n.value );

    return std::get< reward::global_v0 >( global_sing.get() );
  }

  auto get_stats( uint64_t scope ) {
    std::vector< eosio::asset > result;
    reward::stats_table_type    stats_tb{ "reward.libre"_n, scope };

    for ( auto t : stats_tb ) {
      auto p = std::get< reward::stats_v0 >( t.value );

      result.push_back( p.balance );
    }

    return result;
  }

  auto get_providers( eosio::symbol_code symbol_code ) const {
    std::map< eosio::name, std::vector< uint64_t > > result;
    reward::provider_table_type providers_tb{ "reward.libre"_n,
                                              symbol_code.raw() };

    for ( auto t : providers_tb ) {
      auto p = std::get< reward::provider_v0 >( t.value );

      result.insert(
          std::pair{ p.account,
                     std::vector{ p.total_staked, p.claimed, p.unclaimed } } );
    }

    return result;
  };
};