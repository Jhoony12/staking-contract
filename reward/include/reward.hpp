#pragma once

#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>

#include <distributions.hpp>
#include <providers.hpp>

namespace reward {
  class rewards : public eosio::contract {
  public:
    using contract::contract;

    rewards( eosio::name                       receiver,
             eosio::name                       code,
             eosio::datastream< const char * > ds )
        : contract( receiver, code, ds ) {}

    void init( uint32_t                          distribution_interval_sec,
               uint64_t                          reward_per_block,
               std::vector< eosio::symbol_code > allowed_pools );
    void receipthdr( eosio::time_point_sec distribution_time,
                     eosio::asset          reward_per_block,
                     eosio::asset          total_pool_reward );
    void receipt( eosio::time_point_sec distribution_time,
                  std::string          &pool_name,
                  eosio::name           account,
                  double                reward_pct,
                  eosio::asset          reward );
    void setinterval( uint32_t time_sec );
    void setblockrwd( uint64_t amount );
    void addpool( eosio::symbol_code pool );
    void rmpool( eosio::symbol_code pool );

    void claim( eosio::symbol_code pool, eosio::name account );

    void updateall( uint32_t max_steps );
    void migrate( eosio::name account, eosio::symbol_code code );

    void notify_stake( eosio::name account, eosio::asset quantity );
    void notify_withdraw( eosio::name account, eosio::asset quantity );

  private:
  };

  EOSIO_ACTIONS( rewards,
                 "reward.libre"_n,
                 action( init,
                         distribution_interval_sec,
                         reward_per_block,
                         allowed_pools ),
                 action( receipthdr,
                         distribution_time,
                         reward_per_block,
                         total_pool_reward ),
                 action( receipt,
                         distribution_time,
                         pool_name,
                         account,
                         reward_pct,
                         reward ),
                 action( setinterval, time_sec ),
                 action( setblockrwd, amount ),
                 action( addpool, pool ),
                 action( rmpool, pool ),
                 action( claim, pool, account ),
                 action( updateall, max_steps ),
                 notify( "farm.libre"_n, stake ),
                 notify( "farm.libre"_n, withdraw ) )

} // namespace reward