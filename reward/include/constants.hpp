#pragma once

#include <eosio/asset.hpp>

namespace reward {
  inline constexpr auto     base_token_symbol = eosio::symbol( "LIBRE", 4 );
  inline constexpr uint32_t min_distribution_interval_sec = 10800;
} // namespace reward