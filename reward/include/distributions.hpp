#pragma once

#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>
#include <eosio/singleton.hpp>

#include <constants.hpp>
#include <utils.hpp>

namespace reward {
  struct global_v0 {
    uint32_t                          distribution_interval_sec;
    uint64_t                          reward_per_block;
    std::vector< eosio::symbol_code > allowed_pools;
  };
  EOSIO_REFLECT( global_v0,
                 distribution_interval_sec,
                 reward_per_block,
                 allowed_pools )

  using global_variant = std::variant< global_v0 >;
  using global_singleton = eosio::singleton< "global"_n, global_variant >;

  struct pool {
    uint64_t           amount;
    eosio::symbol_code symbol_code;
  };
  EOSIO_REFLECT( pool, amount, symbol_code )

  // TODO: consider updating from std::vector to std::set
  struct next_distribution {
    eosio::time_point_sec distribution_time;
    eosio::asset          total_reward;
    eosio::asset          reward_per_block;
    std::vector< pool >   pools;
  };
  EOSIO_REFLECT( next_distribution, distribution_time )

  struct current_distribution : next_distribution {};
  EOSIO_REFLECT( current_distribution,
                 base next_distribution,
                 total_reward,
                 reward_per_block,
                 pools )

  using distribution_variant =
      std::variant< next_distribution, current_distribution >;
  using distribution_singleton =
      eosio::singleton< "distribution"_n, distribution_variant >;

  struct reward_distribution {
    eosio::symbol_code last_pool;
    eosio::name        last_account;
  };
  EOSIO_REFLECT( reward_distribution, last_pool, last_account )

  using state_variant = std::variant< reward_distribution >;
  using state_singleton = eosio::singleton< "state"_n, state_variant >;

  template < typename T > T *get_if_derived( distribution_variant *state ) {
    return std::visit(
        []( auto &s ) -> T * {
          if constexpr ( std::is_base_of_v< T,
                                            std::decay_t< decltype( s ) > > ) {
            return &s;
          } else {
            return nullptr;
          }
        },
        *state );
  }

  struct stats_v0 {
    eosio::asset balance;

    uint64_t primary_key() const { return balance.symbol.code().raw(); }
  };
  EOSIO_REFLECT( stats_v0, balance )

  using stats_variant = std::variant< stats_v0 >;

  struct stats {
    stats_variant value;
    FORWARD_MEMBERS( value, balance )
    FORWARD_FUNCTIONS( value, primary_key )
  };
  EOSIO_REFLECT( stats, value )

  using stats_table_type = eosio::multi_index< "stats"_n, stats >;

  class distributions {
  private:
    eosio::name            contract;
    global_singleton       global_sing;
    distribution_singleton distribution_sing;
    state_singleton        state_sing;
    stats_table_type       stats_tb;

  public:
    distributions( eosio::name contract )
        : contract( contract ), global_sing( contract, contract.value ),
          distribution_sing( contract, contract.value ),
          state_sing( contract, contract.value ),
          stats_tb( contract, contract.value ) {}

    void      on_init( uint32_t                          distribution_interval_sec,
                       uint64_t                          reward_per_block,
                       std::vector< eosio::symbol_code > allowed_pools );
    void      on_setinterval( uint32_t time_sec );
    void      on_setblockrwd( uint64_t amount );
    void      on_addpool( eosio::symbol_code pool );
    void      on_rmpool( eosio::symbol_code pool );
    bool      setup_distribution();
    void      distribute_reward( uint32_t             &max_steps,
                                 current_distribution &curr_dist,
                                 reward_distribution  &st_cal );
    uint32_t  update_reward( uint32_t max_steps );
    void      add_balance( eosio::asset quantity );
    void      sub_balance( eosio::asset quantity );
    bool      is_next_distribution();
    bool      is_valid_pool( eosio::symbol_code pool );
    void      on_clearall();
    global_v0 global();
  };
} // namespace  reward