# LIBRE Staking and Farming Contracts

<p align="center">
	<a href="https://gitlab.com/libre-chain/staking-contract">
		<img src="https://uploads-ssl.webflow.com/6352f1cf299a9bb136925a0e/649f1a412ee896a46e05c3fb_Column-p-500.jpg" width="300">
	</a>
</p>

![](https://img.shields.io/gitlab/license/libre-chain/staking-contract) ![](https://img.shields.io/badge/code%20style-standard-brightgreen.svg) ![](https://img.shields.io/badge/%E2%9C%93-collaborative_etiquette-brightgreen.svg) ![](https://img.shields.io/twitter/follow/EdeniaWeb3.svg?style=social&logo=twitter) ![](https://img.shields.io/gitlab/forks/libre-chain/staking-contract?style=social)

## Description

This repo contains the source code for the smart contracts of [farm.libre](https://local.bloks.io/account/farm.libre?nodeUrl=https%3A%2F%2Flb.libre.org&coreSymbol=LIBRE&systemDomain=eosio&hyperionUrl=https%3A%2F%2Flb.libre.org), [reward.libre](https://local.bloks.io/account/reward.libre?nodeUrl=https%3A%2F%2Flb.libre.org&coreSymbol=LIBRE&systemDomain=eosio&hyperionUrl=https%3A%2F%2Flb.libre.org), [mint.libre](https://local.bloks.io/account/mint.libre?nodeUrl=https%3A%2F%2Flb.libre.org&coreSymbol=LIBRE&systemDomain=eosio&hyperionUrl=https%3A%2F%2Flb.libre.org) and [stake.libre](https://local.bloks.io/account/stake.libre?nodeUrl=https%3A%2F%2Flb.libre.org&coreSymbol=LIBRE&systemDomain=eosio&hyperionUrl=https%3A%2F%2Flb.libre.org).

## Compilation

This project is configured to be built with [CLSDK](https://gofractally.github.io/contract-lab/book/index.html), so, make sure you have installed CLSDK to compile the contracts in your host environment or using docker to compile in a virtual machine.

### Build with CLSDK

```sh
./build.sh
```

### Build with Docker

```sh
./build-docker.sh
```

Whether you compiled the contract using the CLSDK or Docker, this process will place in each contract root folder the compiled wasm and abi files.

# Contributing

Please Read EOS Costa Rica's [Open Source Contributing Guidelines](https://developers.eoscostarica.io/docs/open-source-guidelines).

Please report bugs big and small by [opening an issue](https://gitlab.com/libre-chain/staking-contract/-/issues)

# About Libre Chain

Bitcoin. Simplified.

[Libre](https://www.libre.org/) is a new blockchain that makes Bitcoin faster, easier, and cheaper to use for anyone.
