# All cmake projects need these
cmake_minimum_required(VERSION 3.16)
project(stakingtoken)

# clsdk requires C++20
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(ENABLE_TESTNET OFF)
if(ENABLE_TESTNET)
    add_definitions(-DENABLE_TESTNET)
endif()

# Libraries for building contracts and tests
find_package(clsdk REQUIRED)

add_subdirectory(stakingtoken)
add_subdirectory(mint)
add_subdirectory(farming)
add_subdirectory(reward)
add_subdirectory(token)